import { MONGO_URI } from '../src/utils/config';
import { ConnectionOptions, connect } from "mongoose";

const connectDB = async () => {
  try {
    const options: ConnectionOptions = {
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true,
      useUnifiedTopology: true
    };
    await connect(
      MONGO_URI,
      options
    );
    console.log("MongoDB Connected...");
  } catch (err) {
    console.error(err.message);
    // Exit process with failure
    process.exit(1);
  }
};

export default connectDB;