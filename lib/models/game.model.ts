import { Schema, model, Model } from 'mongoose';
import { IGameDocument } from './interfaces/game.interface';

interface IGame extends IGameDocument {
  generateAuthToken(): any;
}

interface IGameModel extends Model<IGame> {
  random(): any;
}

export const GameSchema = new Schema({
  title: {
    type: String,
    required: 'Enter game title'
  },
  description: {
    type: String,
    required: 'Enter game description'
  },
  genre: [{
    type: String
  }],
  thumbnail: String,
  image: String,
  homepage: String,
  youtubeTrailer: String,
  amazonRefLink: String,
  subscribers: [
    {
      type: Schema.Types.ObjectId,
      ref: 'user'
    }
  ],
  release_at: {
    type: Date,
    default: Date.now
  }
}, {
  timestamps: true
});

GameSchema.statics.random = async function () {
  this.count((err, count: number) => {
    if (err) {
      throw new Error(err);
    }
    var rand = Math.floor(Math.random() * count);
    this.findOne().skip(rand).exec();
  });
};

const Game: IGameModel = model<IGame, IGameModel>('Game', GameSchema);

export default Game;