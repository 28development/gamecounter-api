import { Request } from 'express';

interface IUserRequest extends Request {
  user: any;
  token: {};
}

export default IUserRequest;