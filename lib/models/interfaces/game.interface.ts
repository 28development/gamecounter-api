import { Document, Model } from 'mongoose';

export interface IGame {
  isAdmin: Boolean,
  name: string;
  email: string;
  password: string;
  avatar: string;
  tokens?: [];
  favorites: [];
  created_at: Date;
  updated_at: Date;
}

export interface IGameDocument extends IGame, Document {
  generateAuthToken(): any;
}

export interface IGameModel extends Model<IGameDocument> {
  random(email: string, password: string): any;
}

export default IGameDocument;