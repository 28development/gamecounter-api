import { Document, Model } from 'mongoose';

export interface IUser {
  isAdmin: Boolean,
  name: string;
  email: string;
  password: string;
  avatar: string;
  tokens?: [];
  favorites: [];
  created_at: Date;
  updated_at: Date;
}

export interface IUserDocument extends IUser, Document {
  generateAuthToken(): any;
}

export interface IUserModel extends Model<IUserDocument> {
  findByCredentials(email: string, password: string): any;
}