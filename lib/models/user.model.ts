import { Schema, model, Model } from 'mongoose';
import { NextFunction } from 'express';
import { JWT_KEY } from '../../src/utils/config';
import validator from 'validator';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { IUserDocument } from 'models/interfaces/user.interface';

interface IUser extends IUserDocument {
  generateAuthToken(): any;
}

interface IUserModel extends Model<IUser> {
  findByCredentials(email: string, password: string): any;
}

const UserSchema: Schema = new Schema({
  isAdmin: Boolean,
  name: {
    type: String,
    required: true,
    trim: true
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    validate: (v: string): any => {
      if (!validator.isEmail(v)) {
        throw new Error('Invalid Email address')
      }
    },
    message: "Please enter a valid email"
  },
  password: {
    type: String,
    required: true,
    minLength: 7
  },
  avatar: {
    type: String
  },
  tokens: [{
    token: {
      type: String,
      required: true
    }
  }],
  favorites: [{
      type: Schema.Types.ObjectId,
      ref: 'Game'
  }],
}, {
  timestamps: true
});

UserSchema.pre<IUserDocument>('save', async function (next: NextFunction) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8) // Hash the password before saving the user model
  }
  next();
});

UserSchema.methods.generateAuthToken = async function () {
  const user = this;
  const token = jwt.sign({ _id: user._id }, JWT_KEY); // Generate an auth token for the user

  user.tokens = user.tokens.concat({ token });
  await user.save();

  return token;
};

UserSchema.statics.findByCredentials = async function(email: string, password: string) {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error('Invalid login credentials');
  }

  const isPasswordMatch = await bcrypt.compare(password, user.password);

  if (!isPasswordMatch) {
    throw new Error('Invalid login credentials');
  }

  return user;
};

const User: IUserModel = model<IUser, IUserModel>('User', UserSchema);

export default User;