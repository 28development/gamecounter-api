import jwt from 'jsonwebtoken';
import { JWT_KEY } from '../../src/utils/config';
import { Request, Response, NextFunction } from 'express';
import User from '../models/user.model';
import IUserRequest from '../models/interfaces/user.request.interface';
import Payload from "../types/Payload";

const auth = async (req: IUserRequest, res: Response, next: NextFunction) => {
  // remove Bearer Space since that is added to the token, we only want to token
  const token = req.header('Authorization').replace('Bearer ', '');
  const msg = { auth: false, message: 'No token provided.' };

  if (!token) res.status(500).send(msg);

  try {
    const payload: Payload | any = jwt.verify(token, JWT_KEY);
    const user = await User.findOne({ _id: payload._id, 'tokens.token': token });

    if (!user) {
      throw new Error("Could not find User");
    }

    req.user = user;
    req.token = token;
    next();
  } catch (error) {
    res.status(401).send({ error: 'Not authorized to access this resource.' });
  }
}

export default auth;