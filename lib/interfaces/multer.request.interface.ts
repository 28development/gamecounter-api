import { Request } from 'express';

interface IMulterRequest extends Request {
  file: any;
  thumbnail: {};
  image: {}
}

export default IMulterRequest;