import { Application, Request, Response } from 'express';
import { UserController } from '../controllers/user.controller';
import auth from '../middleware/auth.middleware';

export default class UserRoutes {

  public UserController: UserController = new UserController();

  public routes(app: Application): void {
    app.route('/api/users')
      .get(auth, this.UserController.all)
      .post(auth, this.UserController.add);

    app.route('/api/users/me')
      .get(auth, this.UserController.me);

    app.route('/api/users/me/logout')
      .post(auth, this.UserController.logout);

    app.route('/api/users/me/logoutall')
      .post(auth, this.UserController.logoutall);

    app.route('/api/users/login')
      .post(this.UserController.login);

    app.route('/api/users/:id')
      .get(this.UserController.getById)
      .put(auth, this.UserController.update)
      .patch(auth, this.UserController.patch)
      .delete(auth, this.UserController.delete)

    app.route('/api/users/:id/favorites/push')
      .patch(auth, this.UserController.pushToFavorites)

    app.route('/api/users/:id/favorites/remove')
      .patch(auth, this.UserController.removeFromFavorites);
  }
}