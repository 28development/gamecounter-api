import { Application } from 'express';
import { GameController } from '../controllers/game.controller';
import auth from '../middleware/auth.middleware';
import upload from '../../src/utils/upload';

export default class GameRoutes {

  public GameController: GameController = new GameController();

  public routes(app: Application): void {
    app.route('/api/game')
      .get(this.GameController.all)
      .post(this.GameController.add);

    app.route('/api/game/random')
      .get(this.GameController.random);

    app.route('/api/game/:id')
      .get(this.GameController.getById)
      .put(auth, this.GameController.update)
      .delete(auth, this.GameController.delete)
  }
}