import Game from '../models/game.model';
import { Request, Response } from 'express';
import IGame from '../models/interfaces/game.interface';
import upload from '../../src/utils/upload';
import IMulterRequest from '../interfaces/multer.request.interface';

export class GameController {

  public all(req: Request, res: Response) {
    Game.find({}, (err: any, game: IGame) => {
      if (err) {
        res.send(err);
      }
      res.json(game);
    });
  }

  public getById(req: Request, res: Response) {
    Game.findById(req.params.id, (err: any, game: IGame) => {
      if (err) {
        res.send(err);
      }
      res.json(game);
    });
  }

  public add(req: IMulterRequest, res: Response) {
    const cpUpload = upload.fields([
      { name: 'image', maxCount: 1 },
      { name: 'thumbnail', maxCount: 1 }
    ]);

    cpUpload(req, res, (err)  => {
      if (err) {
        return res.status(422).send({
          errors: [{ title: 'Image Upload Error', detail: err }]
        });
      }

      req.body.image = req.files['image'][0].location;
      req.body.thumbnail = req.files['thumbnail'][0].location;

      const game = new Game(req.body);

      game.save((err: any, game: IGame) => {
        if (err) {
          res.send(err);
        }
        res.json(game);
      });
    });
  }

  public update(req: Request, res: Response) {
    Game.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err: any, game: IGame) => {
      if (err) {
        res.send(err);
      }
      res.json(game);
    });
  }

  public delete(req: Request, res: Response) {
    Game.deleteOne({ _id: req.params.id }, (err: any) => {
      if (err) {
        res.send(err);
      }
      res.json({ message: 'Successfully deleted game!' });
    });
  }

  public async random(req: Request, res: Response) {
    const game = await Game.aggregate([{ $sample: { size: 1 } }])

    res.json(game);
  }
}