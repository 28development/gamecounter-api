import { IUserModel, IUser } from 'models/interfaces/user.interface';
import User from '../models/user.model';
import { Request, Response, NextFunction } from 'express';
import IUserRequest from '../models/interfaces/user.request.interface';

export class UserController {

  public all(req: Request, res: Response) {
    User.find({}, (err: any, user: IUserModel) => {
      if (err) {
        res.send(err);
      }
      res.json(user);
    });
  }

  public getById(req: Request, res: Response) {
    User.findById(req.params.id, (err: any, user: IUserModel) => {
      if (err) {
        res.send(err);
      }
      res.json(user);
    });
  }

  public add(req: Request, res: Response) {
    const user = new User(req.body);
    user.save().then(() => {
      const token = user.generateAuthToken();
      res.status(201).send({ user, token });
    }).catch(error => {
      res.status(400).send(error.message);
    });
  }

  public async login(req: Request, res: Response) {
    try {
      const { email, password } = req.body;
      const user = await User.findByCredentials(email, password);

      if (!user) {
        return res.status(401).send({ error: 'Login failed! Check authentication credentials' });
      }

      const token = await user.generateAuthToken();
      res.send({ user, token });
    } catch (error) {
      res.status(400).send(error)
    }
  }

  public me(req: IUserRequest, res: Response, next: NextFunction) {
    res.send(req.user);
  }

  public async logout(req: IUserRequest, res: Response) {
    try {
      req.user.tokens = req.user.tokens.filter((token: any) => {
        return token.token != req.token;
      });

      await req.user.save();
      res.send({ message: "Erfolgreich ausgeloggt." });
    } catch (error) {
      res.status(500).send(error);
    }
  }

  public async logoutall(req: IUserRequest, res: Response) {
    try {
      req.user.tokens.splice(0, req.user.tokens.length);
      await req.user.save();
      res.send({ message: "Erfolgreich überall ausgeloggt." });
    } catch (error) {
      res.status(500).send(error);
    }
  }

  public async update(req: Request, res: Response) {
    try {
      const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });
      user.save();
      res.send(user);
    } catch (err) {
      res.status(500).send(err);
    }
  }

  // @todo update every single one ...
  public async patch(req: Request, res: Response) {
    try {
      const user = await User.findByIdAndUpdate(
        req.params.id,
        { $addToSet: { "favorites": req.body.id } },
        { new: true }, (err, result) => {
          console.log(err);
        }
      );

      res.send(user.favorites);
    } catch (err) {
      res.status(500).send(err);
    }
  }

  public async pushToFavorites(req: Request, res: Response) {
    try {
      const user = await User.findByIdAndUpdate(
        req.params.id,
        { $addToSet: { "favorites": req.body.id } },
        { new: true }, (err, result) => {
          console.log(err);
        }
      );

      res.send(user.favorites);
    } catch (err) {
      res.status(500).send(err);
    }
  }

  public async removeFromFavorites(req: Request, res: Response) {
    try {
      const user = await User.findByIdAndUpdate(
        req.params.id,
        { $pull: { "favorites": req.body.id } },
        { new: true }, (err, result) => {
          console.log(err);
        }
      );

      res.send(user.favorites);
    } catch (err) {
      res.status(500).send(err);
    }
  }

  public delete(req: Request, res: Response) {
    User.deleteOne({ _id: req.params.id }, (err: any) => {
      if (err) {
        res.send(err);
      }
      res.json({ message: 'Successfully deleted User!' });
    });
  }

}