import express, { Application } from "express";
import bodyParser from "body-parser";
import GameRoutes from './routes/game.route';
import UserRoutes from './routes/user.route';
import compression from 'compression';
import cors from 'cors';
import helmet from 'helmet';
import connectdb from '../config/database';

class App {

  public app: Application;
  public gameRoutes: GameRoutes = new GameRoutes();
  public userRoutes: UserRoutes = new UserRoutes();

  constructor() {
    this.app = express();
    this.config();
    this.setRoutes();
    connectdb();
  }

  private setRoutes(): void {
    this.gameRoutes.routes(this.app);
    this.userRoutes.routes(this.app);
  }

  private config(): void {
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false })); // sup application/x-www-form-urlencoded post data
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(cors());
  }

}

export default new App().app;