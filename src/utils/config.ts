import * as dotenv from "dotenv";

dotenv.config();
let path: string;

switch (process.env.NODE_ENV) {
  case "test":
    path = `${__dirname}/../../.env.test`;
    break;
  case "production":
    path = `${__dirname}/../../.env.production`;
    break;
  default:
    path = `${__dirname}/../../.env.development`;
}

dotenv.config({ path: path });

export const APP_ID = process.env.APP_ID;
export const LOG_LEVEL = process.env.LOG_LEVEL;
export const MONGO_URI = process.env.MONGO_URI;
export const PORT = process.env.PORT;
export const JWT_KEY = process.env.JWT_KEY;
export const AWS_ACCESS_KEY_ID = process.env.S3_ACCESS_KEY_ID;
export const AWS_SECRET_KEY = process.env.S3_SECRET_KEY;